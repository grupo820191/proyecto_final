#include <math.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */

typedef struct kvTDA{
    char* clave; //clave
    char* valor; //el objeto (puntero)
}kvObjeto;

typedef struct kvStoreTDA{
    char* id; //Id de la hashtable
    int elementos; //Numero de elemento en la hashtable
    int numeroBuckets; //Numero de buckets
    kvObjeto** buckets; //Arreglo de buckets (arreglo de punteros a objetos)(items)
}kvStore;

kvStore* crearkvStore(char* id,int numeroBuckets);
kvObjeto* crearkvObjeto(char* clave,char* valor);
void kvPut(kvStore* kvs,char* key,char* value);
char* kvGet(kvStore* kvs,char* key);
void kvRemove(kvStore* kvs,char* key);
void vaciarBucket(kvObjeto* obj);
void kvDelete_kv(kvStore* kvs);
unsigned long hash(unsigned char *str);
int buscarBucket(kvStore* kvs,char* str);
//int kvStatus(kvStore* kvs);