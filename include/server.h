#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	

#define QUEUESIZE 10
#define LONGITUD 4096

typedef struct servidor{
    in_addr_t ip;
    int puerto;
    int socketEscuchador;
    //deprecated:int socketConectado; 
    struct sockaddr_in direccion_servidor;
} Servidor;

Servidor*  crearServidor(char* ip,int puerto);
int levantarServidor(Servidor* servidor);