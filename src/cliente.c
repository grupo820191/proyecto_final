#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	

#define QUEUESIZE 10
#define LONGITUDRUTA 20
#define MAXDATASIZE 100
int numbytes;
char buf[MAXDATASIZE];

//Main

int main( int argc, char *argv[]) { 

	char* ruta[LONGITUDRUTA];
	ruta[0]= "PUT,primero,clave,valor";
	ruta[1]= "PUT,segundo,clave,valor";
	ruta[2]= "PUT,tercero,clave,valor";
	ruta[3]= "PUT,segundo,clave,valor";
	ruta[4]= "GET,primero,clave";
	ruta[5]= "PUT,segundo,clave,valor";
	ruta[6]= "PUT,primero,clave,valor";
	ruta[7]= "PUT,primero,clave,valor";
	ruta[8]= "REMOVE,segundo,clave,valor";
	ruta[9]= "PUT,primero,clave,valor";
	ruta[10]= "DELETE,tercero";
	ruta[11]= "PUT,primero,clave,valor";
	ruta[12]= "PUT,primero,clave,valor";
	ruta[13]= "PUT,segundo,clave,valor";
	ruta[14]= "PUT,primero,clave,valor";
	ruta[15]= "PUT,segundo,clave,valor";
	ruta[16]= "PUT,primero,clave,valor";
	ruta[17]= "PUT,primero,clave,valor";
	int puerto = 4444;
	//Direccion del servidor
	struct sockaddr_in direccion_destino;

	memset(&direccion_destino, 0, sizeof(direccion_destino));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_destino.sin_family = AF_INET;		//IPv4
	direccion_destino.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_destino.sin_addr.s_addr = inet_addr("127.0.0.1") ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

	int fd;
	
	if((fd = socket(direccion_destino.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear socket\n");
		return -1;
	}
		
	int res = connect(fd, (struct sockaddr *)&direccion_destino, sizeof(direccion_destino));

	if(res < 0){
		close(fd);
		perror("Error al conectar\n");
		return -1;
	}
	
	send(fd, ruta[atoi(argv[1])], 40,0);

	numbytes = recv(fd,buf,MAXDATASIZE,0);
	
	buf[numbytes]='0';
	printf("Mensaje del servidor:%n",buf);
	close(fd);
		
}



