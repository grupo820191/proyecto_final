#include "server.h"

Servidor*  crearServidor(char* ip,int puerto){
    Servidor* servidor = (Servidor *)malloc(sizeof(Servidor)); //falta liberar la memoria al final cerrarServidor() o algo asi

    servidor->ip     = inet_addr(ip);
    servidor->puerto = puerto;

    memset(&servidor->direccion_servidor, 0, sizeof(struct sockaddr_in)); //aqui creo que podria tener problemas por lo que le pasa  la direccion, creo que seria la direccion despues de->

	servidor->direccion_servidor.sin_family = AF_INET;
	servidor->direccion_servidor.sin_port = htons(servidor->puerto);
    servidor->direccion_servidor.sin_addr.s_addr = servidor->ip;

    levantarServidor(servidor);
	return servidor;

}

int levantarServidor(Servidor* servidor){
	
	printf("creando Socket\n");
	if((servidor->socketEscuchador = socket(servidor->direccion_servidor.sin_family, SOCK_STREAM, 0)) < 0){
		perror("Error al crear socket\n");
		return -1;
	}
	
	printf("creando enlace (Bind)\n");
	if(bind(servidor->socketEscuchador, (struct sockaddr *)&servidor->direccion_servidor, sizeof(servidor->direccion_servidor)) < 0){
		perror("Error en bind\n");
		return -1;	
	}

	if(listen(servidor->socketEscuchador, QUEUESIZE) < 0){
		perror("Error en listen\n");
		return -1;
	}
	printf("el servidor esta listo para escuchar en %d:%d\n",servidor->socketEscuchador,servidor->puerto);

	return 1;
}

