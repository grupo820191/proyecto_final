#include <stdlib.h>
//#include <string.h>

#include "kvStore.h"

kvObjeto* crearkvObjeto( char* clave,  char* valor){
    kvObjeto* obj = malloc(sizeof(kvObjeto));
    obj->clave = clave;
    obj->valor = valor;
    return obj;
}

kvStore* crearkvStore(char* id,int numeroBuckets) {
    kvStore* kvs = malloc(sizeof(kvStore));

    kvs->id = id;
    kvs->numeroBuckets = numeroBuckets;
    kvs->elementos = 0;
    kvs->buckets = calloc((size_t)kvs->numeroBuckets, sizeof(kvObjeto*));
    return kvs;
}

void vaciarBucket(kvObjeto* obj) {
    free(obj);
}

void kvDelete_kv(kvStore* kvs) {
    for (int i = 0; i < kvs->numeroBuckets; i++) {
        kvObjeto* obj = kvs->buckets[i];
        if (obj != NULL) {
            vaciarBucket(obj);
        }
    }
    free(kvs->buckets);
    free(kvs);
}

unsigned long hash(unsigned char *str){
    unsigned long hash = 5381;
    int c;

    while ((c = *str++)){
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}

int buscarBucket(kvStore* kvs,char* str){
    return abs(hash((unsigned char*)str))%kvs->numeroBuckets;
}

void kvPut(kvStore* kvs, char* clave, char* valor) {
    kvObjeto* obj = crearkvObjeto(clave, valor);
    int index = buscarBucket(kvs,obj->clave);
    kvObjeto* guardado = kvs->buckets[index];

    if(guardado == NULL){
        vaciarBucket(kvs->buckets[index]);
    }
    
    kvs->buckets[index] = obj;
    kvs->elementos++;
}

char* kvGet(kvStore* kvs, char* clave) {
    int index = buscarBucket(kvs,clave);
    kvObjeto* obj = kvs->buckets[index];

    if (obj != NULL) {
        return obj->valor;
    }
    printf("esta vacio");
    return NULL;
}

void kvRemove(kvStore* kvs, char* clave) {
    int index = buscarBucket(kvs,clave);
    kvObjeto* obj = kvs->buckets[index];

    if (obj != NULL) {
        vaciarBucket(obj);
        kvs->buckets[index] = NULL;
        kvs->elementos--;
    } 
}