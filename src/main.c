#include <stdio.h>
#include "server.h"
#include "kvStore.h"
#include <pthread.h>
#include <semaphore.h>

void * rutina_atender(int socketConectado,int entrada_numBuckets,kvStore ** arregloKVS);

typedef struct instruccion{
    int cantidad;
    char* instruct;
    char* kv_id;
    char* clave;
    char* valor;
}Instruccion;

typedef struct parametros{
    int socketConectado;
	int entrada_numBuckets;
	kvStore ** arregloKVS;
}Parametros;

Servidor * servidor;
kvStore * arregloKVS[100]={0};
int main(int argc, char *argv[]) {
	
	arregloKVS[0] = crearkvStore("primero",100);	

	int tieneTresArgumentos = (argc == 1 || argc == 2 || argc == 3);
	int tieneMasDeCuatro    = (argc != 4);

	if(tieneTresArgumentos){
		printf("Uso: ./kvstore <Ip> <Puerto> <NumeroDeBuckets>\n");
		exit(-1);
	}

	if(tieneMasDeCuatro){
		printf( "por favor especificar un ip, un puerto y el numero de buskets\n");
        printf("Uso: ./kvstore <Ip> <Puerto> <NumeroDeBuckets>\n");
        exit(-1);
	}
	

	char* entrada_ip = argv[1];
	int entrada_puerto = atoi(argv[2]);
	int entrada_numBuckets = atoi(argv[3]);

    servidor = crearServidor(entrada_ip,entrada_puerto);
	 
	Parametros param ={0};
	while(1){
		param.socketConectado = accept(servidor->socketEscuchador, NULL, 0);
		param.arregloKVS=arregloKVS;
		param.entrada_numBuckets=entrada_numBuckets;
		
	
		rutina_atender(param.socketConectado,entrada_numBuckets,arregloKVS);//aqui empieza la rutina
		//pthread_create(&h,NULL,rutina_atender,NULL);
    }
}

void * rutina_atender(int socketConectado,int entrada_numBuckets,kvStore ** arregloKVS){
		char instruccionCliente[100] = {0};
		int bytesLeidos = read(socketConectado, instruccionCliente, 100);//aqui toma los comandos del cliente
		if(bytesLeidos < 0){
			perror("Error al leer\n");		
		}

		else{
			//--
			printf("Recibimos: %s\n", instruccionCliente);
			/* split de la instruccion del cliente --------- [inst][kvid][clave][valor]*/
			char * comando[100];
			char* parteInst = strtok(instruccionCliente,",");
			comando[0] = parteInst;
			int i = 1;
			while(parteInst != NULL){
				parteInst = strtok(0,",");
                comando[i]= parteInst;
                i++;
			}
			if(comando[0] == NULL){
				write(socketConectado,"error de sintaxis",18);
			}
			//--
			/*
				buscar y seleccionar el kvstore pedido
			*/
			
			kvStore * seleccionado = arregloKVS[0];
			Instruccion inst;
			inst.instruct=comando[0];
			inst.kv_id=comando[1];

			int is_encontrado = 0;
			kvStore * buf;
			for(int i =0;i< entrada_numBuckets;i++){
				if(arregloKVS[i]==NULL){
					buf=arregloKVS[i];
                    //printf("listaKvStores[%d] : vacio\n",i);
				}
				else if(strcmp(arregloKVS[i]->id,inst.kv_id)==0){//falta comprobar si existe
					seleccionado = arregloKVS[i];
					is_encontrado=1;
                    printf("encontre el kvstore alojado en: listaKvStores[%d]\n",i);
				}
				else{
                    printf("listaKvStores[%d] : %s\n",i,arregloKVS[i]->id);
                }
			}
			if(!is_encontrado && strcmp(inst.instruct,"PUT")==0){
					buf =crearkvStore(inst.kv_id,100);	
					seleccionado = buf;
					printf("creare un kv ya que esta vacio\n");
					write(socketConectado,"no existia, asi que lo creamos para ti c:",42);
			}
			//--

			
			if(strcmp(inst.instruct,"PUT")==0){
				inst.cantidad=3;
				inst.clave=comando[2];
				inst.valor=comando[3];

				if(seleccionado != NULL){
					kvPut(seleccionado,inst.clave,inst.valor);
					printf("objeto insertado\n");
					write(socketConectado,"objeto insertado",17);
				}
				
			}

			if(strcmp(inst.instruct,"GET")==0){
				if(seleccionado != NULL){
					inst.cantidad=2;
					inst.clave=comando[2];
				
        			char* valorDevuelto = kvGet(seleccionado,inst.clave);
					printf("objeto geteado %s\n",valorDevuelto);
					write(socketConectado,valorDevuelto,255);
				}
			}
			
			if(strcmp(inst.instruct,"REMOVE")==0){
				if(seleccionado != NULL){
					inst.cantidad=2;
					inst.clave=comando[2];
					kvRemove(seleccionado,inst.clave);
					printf("objeto removido\n");
					write(socketConectado,"objeto removido",16);
				}
			}

			if(strcmp(inst.instruct,"DELETE")==0){
				if(seleccionado != NULL){
					inst.cantidad=1;
					inst.kv_id=comando[1];
					kvDelete_kv(seleccionado);
					seleccionado = NULL;
					printf("mataste un KvStore :c\n");
					write(socketConectado,"mataste un KvStore :c",22);
				}		
			}
		}
		close(socketConectado);
}
