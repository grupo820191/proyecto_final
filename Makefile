bin/kvstore: obj/main.o obj/server.o  obj/kvstore.o bin/cliente bin/pruebaKVS
	gcc -Wall obj/kvstore.o obj/server.o obj/main.o -pthread -o bin/kvstore

obj/server.o: src/server.c 
	gcc -Wall -c -Iinclude/ src/server.c -pthread -o obj/server.o

obj/kvstore.o: src/kvStore.c
	gcc -Wall -c -Iinclude/ src/kvStore.c -pthread -o obj/kvstore.o

obj/main.o: src/main.c
	gcc -Wall -c -Iinclude/ src/main.c -pthread -o obj/main.o



bin/cliente: obj/cliente.o
	gcc -Wall obj/cliente.o -pthread -o bin/cliente

obj/cliente.o: src/cliente.c 
	gcc -Wall -c -Iinclude/ src/cliente.c -pthread -o obj/cliente.o



bin/pruebaKVS: obj/pruebaKVS.o
	gcc -Wall obj/pruebaKVS.o obj/kvstore.o -pthread -o bin/pruebaKVS

obj/pruebaKVS.o: src/pruebaKVS.c 
	gcc -Wall -c -Iinclude/ src/pruebaKVS.c -pthread -o obj/pruebaKVS.o



.PHONY: clean

clean:
	rm bin/* obj/*
